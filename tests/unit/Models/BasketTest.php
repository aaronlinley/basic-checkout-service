<?php

use App\Collections\OfferCollection;
use App\Helpers\MathsHelper;
use App\Models\Offer;
use App\Models\User;
use App\Models\Basket;

class BasketTest extends \PHPUnit\Framework\TestCase
{
    public function testConstructionWithUser()
    {
        $user = $this->getMockBuilder(User::class)
            ->getMock();

        $basket = new Basket($user);

        $this->assertInstanceOf(Basket::class, $basket);
    }

    public function testAddProductByCode()
    {
        $user = $this->getMockBuilder(User::class)
            ->getMock();

        $basket = new Basket($user);

        $basket->addProductByCode('P001');
        $products = $basket->getProducts()->getProducts();

        $this->assertArrayHasKey('P001', $products);
        $this->assertCount(1, $products);

        // check we can't add the same product multiple times
        $basket->addProductByCode('P001');
        $this->assertCount(1, $products);
    }

    public function testGetTotal()
    {
        $user = $this->getMockBuilder(User::class)
            ->getMock();

        $basket = new Basket($user);

        $basket->addProductByCode('P001');

        $this->assertEquals(200, $basket->getTotal());

        $mockOffer = $this->getMockBuilder(Offer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockOffer->method('getId')->willReturn("1");
        $mockOffer->method('getType')->willReturn("percentage");
        $mockOffer->method('getAmount')->willReturn(10);
        $mockOffer->method('getDescription')->willReturn("10% off");
        $mockOffer->method('subtractOfferFrom')->willReturn(
            MathsHelper::subtractPercentageFromValue($mockOffer->getAmount(), 200)
        );

        $user = $this->getMockBuilder(User::class)
            ->setConstructorArgs([[1]])
            ->getMock();

        $user->method('hasOffers')->willReturn(true);
        $user->method('getOffers')->willReturn([
            $mockOffer
        ]);

        $basket = new Basket($user);

        $basket->addProductByCode('P001');

        $this->assertEquals(180, $basket->getTotal());
    }
}