<?php

namespace Tests\Unit\Collections;

use App\Collections\OfferCollection;
use App\Collections\ProductCollection;
use App\Models\Offer;
use App\Models\Product;

class ProductCollectionTest extends \PHPUnit\Framework\TestCase
{
    public function testAddAndRemoveWithValidData()
    {
        $product = $this->getMockBuilder(Product::class)
            ->disableOriginalConstructor()
            ->getMock();

        $productCollection = new ProductCollection();
        $productCollection->add($product);

        $this->assertEqualsCanonicalizing([$product], $productCollection->getProducts());

        $productCollection->remove($product);

        $this->assertEmpty($productCollection->getProducts());
    }

    public function testAddWithInvalidData()
    {
        $this->expectException(\TypeError::class);

        $mockCollection = $this->getMockBuilder(OfferCollection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $productCollection = new ProductCollection();
        $productCollection->add($mockCollection);
    }

    public function testHas()
    {
        $product = $this->getMockBuilder(Product::class)
            ->setConstructorArgs(['P001'])
            ->getMock();

        $product->method('getCode')->willReturn('P001');

        $productTwo = $this->getMockBuilder(Product::class)
            ->setConstructorArgs(['P002'])
            ->getMock();

        $productTwo->method('getCode')->willReturn('P002');

        $productCollection = new ProductCollection();
        $productCollection->add($product);

        $this->assertTrue($productCollection->has($product));
        $this->assertFalse($productCollection->has($productTwo));
    }
}