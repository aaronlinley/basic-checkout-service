<?php

namespace Tests\Unit\Collections;

use App\Collections\OfferCollection;
use App\Models\Offer;
use App\Models\Product;

class OfferCollectionTest extends \PHPUnit\Framework\TestCase
{
    public function testAddAndRemoveWithValidData()
    {
        $offer = $this->getMockBuilder(Offer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $offer->method('getId')->willReturn("1");

        $offerCollection = new OfferCollection();
        $offerCollection->add($offer);

        $this->assertEqualsCanonicalizing([$offer], $offerCollection->getOffers());

        $offerCollection->remove($offer);

        $this->assertEmpty($offerCollection->getOffers());
    }

    public function testAddWithInvalidData()
    {
        $this->expectException(\TypeError::class);

        $mockCollection = $this->getMockBuilder(OfferCollection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $offerCollection = new OfferCollection();
        $offerCollection->add($mockCollection);
    }
}