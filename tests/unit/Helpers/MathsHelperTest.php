<?php

use App\Helpers\MathsHelper;

class MathsHelperTest extends \PHPUnit\Framework\TestCase
{
    public function testSubtractPercentageFromValue()
    {
        $result = MathsHelper::subtractPercentageFromValue(30, 100);

        $this->assertEquals(70, $result);
    }
    public function testSubtractNumberFromValue()
    {
        $result = MathsHelper::subtractNumberFromValue(5, 25);

        $this->assertEquals(20, $result);
    }
}