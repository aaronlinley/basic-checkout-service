# Basic Checkout Service

This checkout system allows users to pay upfront for products added to their property management agreement.
The system should also allow users to take advantage of special offers.

## Running the project

[Composer](https://getcomposer.org/) and [Docker](https://docs.docker.com/) are required to run the project.

1. `composer install`
2. Bring up the container using `docker compose up -d` - this will start the service in the background
3. Run the checkout using `docker exec -it [CONTAINER_ID] php bin/checkout` - you can pass params to this for user offers and products in the checkout. See the comments of `bin/checkout`.

## Data

Offer IDs and Product Codes for the checkout can be found with more info in the `src/Data` directory, or the tables below:

### Offers
| ID  | Description                        |
|-----|------------------------------------|
| 1   | 10% discount off the basket total  |

### Products
| Product Code | Name             | Price |
|--------------|------------------|-------|
| P001         | Photography      | 200   |
| P002         | Floorplan        | 100   |
| P003         | Gas Certificate  | 83.50 |
| P004         | EICR Certificate | 51.00 |