<?php

namespace App\Helpers;

class MathsHelper
{
    /**
     * @param int|float $percent
     * @param int|float $value
     * @return int|float
     */
    public static function subtractPercentageFromValue(int|float $percent, int|float $value): int|float
    {
        $decimal = $percent / 100;
        $calculatedPercent = $value * $decimal;

        return $value - $calculatedPercent;
    }

    /**
     * @param int|float $toSubtract
     * @param int|float $value
     * @return int|float
     */
    public static function subtractNumberFromValue(int|float $toSubtract, int|float $value): int|float
    {
        return $value - $toSubtract;
    }
}
