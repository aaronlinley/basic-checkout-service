<?php

namespace App\Traits;

trait AccessesData
{
    /**
     * @return string
     */
    private function getDataDirectory(): string
    {
        return dirname(__DIR__) . "/Data";
    }
}
