<?php

namespace App\Collections;

use App\Interfaces\CollectionableInterface;
use App\Interfaces\CollectionInterface;
use App\Models\Offer;

class OfferCollection implements CollectionInterface
{
    /**
     * @var Offer[]
     */
    private array $offers = [];

    /**
     * @inheritDoc
     */
    public function add(CollectionableInterface $element): OfferCollection
    {
        /** @var Offer $element */
        $this->offers[$element->getId()] = $element;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function remove(CollectionableInterface $element): OfferCollection
    {
        /** @var Offer $element */
        if (count($this->getOffers())) {
            if (isset($this->offers[$element->getId()])) {
                unset($this->offers[$element->getId()]);
            }
        }

        return $this;
    }


    /**
     * @return Offer[]
     */
    public function getOffers(): array
    {
        return $this->offers;
    }
}
