<?php

namespace App\Collections;

use App\Interfaces\CollectionableInterface;
use App\Interfaces\CollectionInterface;
use App\Models\Product;

class ProductCollection implements CollectionInterface
{
    /**
     * @var Product[]
     */
    private array $products = [];

    /**
     * @inheritDoc
     */
    public function add(CollectionableInterface $element): ProductCollection
    {
        /** @var Product $element */
        $this->products[$element->getCode()] = $element;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function remove(CollectionableInterface $element): ProductCollection
    {
        /** @var Product $element */
        if (count($this->getProducts())) {
            if (isset($this->products[$element->getCode()])) {
                unset($this->products[$element->getCode()]);
            }
        }

        return $this;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function has(Product $product): bool
    {
        $hasProduct = false;

        if (count($this->getProducts())) {
            foreach ($this->products as $item) {
                if ($item->getCode() === $product->getCode()) {
                    $hasProduct = true;
                }
            }
        }

        return $hasProduct;
    }
}
