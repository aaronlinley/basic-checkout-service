<?php

namespace App\Interfaces;

interface ProductInterface
{
    /**
     * @param string $code
     * @return ProductInterface
     */
    public function setCode(string $code): ProductInterface;

    /**
     * @param string $name
     * @return ProductInterface
     */
    public function setName(string $name): ProductInterface;

    /**
     * @param int|float $price
     * @return ProductInterface
     */
    public function setPrice(int|float $price): ProductInterface;

    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return float
     */
    public function getPrice(): float;
}
