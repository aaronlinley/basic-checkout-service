<?php

namespace App\Interfaces;

interface CollectionInterface
{
    /**
     * Add a new element to the collection
     *
     * @param CollectionableInterface $element
     * @return CollectionInterface
     */
    public function add(CollectionableInterface $element): CollectionInterface;

    /**
     * Remove an element from the collection
     *
     * @param CollectionableInterface $element
     * @return CollectionInterface
     */
    public function remove(CollectionableInterface $element): CollectionInterface;
}
