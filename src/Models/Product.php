<?php

namespace App\Models;

use Exception;
use App\Interfaces\CollectionableInterface;
use App\Interfaces\ProductInterface;
use App\Traits\AccessesData;

class Product implements ProductInterface, CollectionableInterface
{
    use AccessesData;

    /**
     * @var string
     */
    private string $code;

    /**
     * @var string
     */
    private string $name;

    /**
     * @var float
     */
    private float $price;

    /**
     * @param string $code
     */
    public function __construct(string $code)
    {
        $product = $this->getByCode($code);

        if ($product) {
            $name = isset($product['name']) ? (string) $product['name'] : '';
            $price = isset($product['price']) ? floatval($product['price']) : 0;
            $this->setCode($code)
                ->setName($name)
                ->setPrice($price);
        }
    }

    /**
     * @inheritDoc
     */
    public function setCode(string $code): Product
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setName(string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setPrice(int|float $price): Product
    {
        $this->price = floatval($price);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param string $code
     * @return array<string, float|int|string>|null
     */
    private function getByCode(string $code): ?array
    {
        if ($data = file_get_contents($this->getDataDirectory() . "/products.json")) {
            /** @var array<string, array<string, float|int|string>> $products */
            $products = json_decode($data, true);

            if (!isset($products[$code])) {
                throw new Exception("Product not found");
            }

            return $products[$code];
        }

        return null;
    }
}
