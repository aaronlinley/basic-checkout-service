<?php

namespace App\Models;

use Exception;
use App\Interfaces\CollectionableInterface;
use App\Traits\AccessesData;
use App\Helpers\MathsHelper;

class Offer implements CollectionableInterface
{
    use AccessesData;

    /**
     * @var string
     */
    private string $id;

    /**
     * @var string
     */
    private string $description;

    /**
     * @var string
     */
    private string $type;

    /**
     * @var float|int
     */
    private float|int $amount;

    /**
     * @param string $id
     * @throws Exception
     */
    public function __construct(string $id)
    {
        $offer = $this->getByID($id);

        $description = isset($offer['description']) ? (string) $offer['description'] : '';
        $type = isset($offer['type']) ? (string) $offer['type'] : '';
        $amount = isset($offer['amount']) ? floatval($offer['amount']) : 0;

        $this->setId($id)
            ->setDescription($description)
            ->setType($type)
            ->setAmount($amount);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId(string $id): Offer
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): Offer
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): Offer
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return float|int
     */
    public function getAmount(): float|int
    {
        return $this->amount;
    }

    /**
     * @param float|int $amount
     * @return $this
     */
    public function setAmount(float|int $amount): Offer
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @param string $id
     * @return array<string, float|int|string>|null
     * @throws Exception
     */
    private function getByID(string $id): ?array
    {
        if ($data = file_get_contents($this->getDataDirectory() . "/offers.json")) {
            /** @var array<string, array<string, float|int|string>> $offers */
            $offers = json_decode($data, true);

            if (!isset($offers[$id])) {
                throw new Exception("Offer not found");
            }

            return $offers[$id];
        }

        return null;
    }

    /**
     * @param float $total
     * @return float
     * @throws Exception
     */
    public function subtractOfferFrom(float $total): float
    {
        switch ($this->type) {
            case 'percentage':
                // get maths helper and do percentage calc
                $total = MathsHelper::subtractPercentageFromValue($this->getAmount(), $total);
                break;
            case 'minus':
                // get maths helper and do minus calc
                $total = MathsHelper::subtractNumberFromValue($this->getAmount(), $total);
                break;
            default:
                throw new Exception("Offer type invalid");
        }

        return $total;
    }
}
