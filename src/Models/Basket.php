<?php

namespace App\Models;

use Exception;
use App\Collections\ProductCollection;

class Basket
{
    /**
     * @var User
     */
    private User $user;

    /**
     * @var ProductCollection|null
     */
    private ?ProductCollection $products = null;

    /**
     * @var float
     */
    private float $total = 0.00;

    /**
     * @var array<string, Product>
     */
    private array $cachedProducts = [];

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Add a product to the basket, if it's not already there.
     *
     * @param string $code
     * @return $this
     */
    public function addProductByCode(string $code): Basket
    {
        // Get product by code
        $product = new Product($code);

        // If it's not already in the basket, add it
        if (!$this->getProducts()->has($product)) {
            $this->getProducts()->add($product);
        }

        return $this;
    }

    /**
     * Get the basket's current total.
     *
     * Implements some basic thread caching for optimisation.
     *
     * @return float
     * @throws Exception
     */
    public function getTotal(): float
    {
        $products = $this->getProducts()->getProducts();

        // If we have more products than are cached...
        if (count($products) > count($this->cachedProducts)) {
            foreach ($products as $product) {
                // ...for only the products that aren't cached...
                if (!isset($this->cachedProducts[$product->getCode()])) {
                    // ...add their total to the basket...
                    $this->total += $product->getPrice();

                    // ...and cache for future use.
                    $this->cachedProducts[$product->getCode()] = $product;
                }
            }
        }

        // If our user has offers available
        if ($this->getUser()->hasOffers()) {
            foreach ($this->getUser()->getOffers() as $offer) {
                $this->total = $offer->subtractOfferFrom($this->total);
            }
        }

        return $this->total;
    }

    /**
     * @return User
     */
    private function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return ProductCollection
     */
    public function getProducts(): ProductCollection
    {
        if ($this->products === null) {
            $this->products = new ProductCollection();
        }

        return $this->products;
    }
}
