<?php

namespace App\Models;

use Exception;
use App\Collections\OfferCollection;

class User
{
    private OfferCollection $offers;

    /**
     * @param string[] $offerIds
     * @throws Exception
     */
    public function __construct(array $offerIds = [])
    {
        $this->offers = new OfferCollection();

        if (count($offerIds)) {
            foreach ($offerIds as $offerId) {
                $this->offers->add(new Offer($offerId));
            }
        }
    }

    public function hasOffers(): bool
    {
        return count($this->offers->getOffers()) > 0;
    }

    /**
     * @return Offer[]
     */
    public function getOffers(): array
    {
        return $this->offers->getOffers();
    }
}
